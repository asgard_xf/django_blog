from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect

from .models import Article, Comment
from .forms import CommentForm

def getCommentForm():

    from django.forms import modelform_factory
    from django.forms.widgets import HiddenInput
    return modelform_factory(
            Comment, fields=("comment_text", "article"),
            widgets={"article": HiddenInput()}
        )

class ArticleList(ListView):
    model = Article

class ArticleDetail(DetailView):
    model = Article
    template_name = 'xblog/article_detail.html'
    context_object_name = 'article'

    def get_context_data(self, **kwargs):
        context = super(ArticleDetail, self).get_context_data(**kwargs)
        context['article'] = self.object
        comment =  CommentForm()
        comment.fields["article"].initial = self.object
        context['create_comment_form'] =  comment
        context['comment_list'] = self.object.comment_set.all()
        return context

class EntityCreate(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy('reg:auth_login')
    redirect_field_name = 'redirect_to'
    def form_valid(self, form):
        from django.utils import timezone
        form.instance.author = self.request.user
        form.instance.pub_date = timezone.now()
        form.save()
        return super(EntityCreate, self).form_valid(form)

class ArticleCreate(EntityCreate):
    model = Article
    fields = ['image', 'article_text']

class CommentCreate(EntityCreate):
    form_class = CommentForm
    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            self.form_valid(form)
            return HttpResponseRedirect(form.instance.get_absolute_url())
