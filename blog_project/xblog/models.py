from django.conf import settings
from django.db import models
from django.urls import reverse

class PublishedEntity(models.Model):
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )

    pub_date = models.DateTimeField()

    class Meta:
        abstract = True

class Article(PublishedEntity):
    article_text = models.TextField()
    image = models.ImageField(upload_to='images')

    def get_absolute_url(self):
        return reverse('article-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.article_text

    def create_preview(self):
        from PIL import Image
        i = Image.open(self.image.file.name)
        i.thumbnail((100, 100))
        i.save(self.image.file.name + '.min', 'png')

    def get_preview_url(self):
        from django.contrib.staticfiles.templatetags.staticfiles import static
        from os.path import exists
        im = self.image.url + '.min'
        if not exists(im):
            self.create_preview()
        return static( 'xblog/'+im)


class Comment(PublishedEntity):
    comment_text = models.TextField()
    article = models.ForeignKey(
        Article,on_delete=models.CASCADE,
    )

    def get_absolute_url(self):
        return reverse('article-detail', kwargs={'pk':self.article.pk})