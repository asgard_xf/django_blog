from django.forms import ModelForm
from .models import Comment
from django.forms.widgets import HiddenInput, TextInput

class CommentForm(ModelForm):
	class Meta:
		model = Comment
		fields = ['article', 'comment_text']
	def __init__(self, *args, **kwargs):
		from django.forms.widgets import HiddenInput
		super(CommentForm, self).__init__(*args, **kwargs)
		self.fields['article'].widget = HiddenInput()