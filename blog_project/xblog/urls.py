from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.ArticleList.as_view(), name='index'),
    url(r'^article/(?P<pk>[0-9]+)/$', views.ArticleDetail.as_view(), name='article-detail'),
    url(r'^article/add/$', views.ArticleCreate.as_view(), name='article-add'),
    url(r'^comment/add/$', views.CommentCreate.as_view(), name='comment-add'),
]