from django.contrib import admin
from django.utils.html import format_html

from .models import Article, Comment

class CommentsInline(admin.TabularInline):
    model = Comment

class ArticleAdmin(admin.ModelAdmin):
    #change_list_template = 'admin/xblog/article/change_list.html'
    fields = ['pub_date', 'article_text','author']

    list_display =['article', 'preview']

    inlines = [CommentsInline]

    def preview(self, article):
        return format_html('<img src="{}" style="max-width: 200px;">', article.get_preview_url())

    def article(self, article):
        return article.article_text


admin.site.register(Article, ArticleAdmin)
admin.site.register(Comment)